import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import * as Sentry from '@sentry/browser';

Sentry.init({dsn: "https://c0851f58a2224148a95ab982b1cfbc89@sentry.io/1727276"});

import App from './App';

ReactDOM.render(<HashRouter>
        <App />
    </HashRouter>,
  document.getElementById('root'));
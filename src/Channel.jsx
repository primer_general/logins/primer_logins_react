// @flow
import React from 'react';
import {graphql, createFragmentContainer} from 'react-relay';
import { RecipeCard } from 'react-ui-cards';

import type from './__generated__/Channel_item.graphql'

const Channel = (props) => {
    return  <RecipeCard
                className='card-container'
                href='#/channel'
                thumbnail={props.item.poster}
                title={props.item.name}
                time={props.item.duration}
                servings={props.item.ages}
                likeCallback={() => alert('Added to favourites')} />
}

export default createFragmentContainer(
Channel,
{
    item:  graphql`
        fragment Channel_item on Channel {
            name
            description
            poster
            address
        }`
});
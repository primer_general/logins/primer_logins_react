// @flow
import {
    Environment,
    Network,
    RecordSource,
    Store,
    QueryResponseCache
  } from 'relay-runtime';
import { RelayNetworkLayer, 
        urlMiddleware, 
        authMiddleware,
        cacheMiddleware 
} from 'react-relay-network-modern';

  const SERVER = 'http://localhost:4000';

  const network = new RelayNetworkLayer(
    [
      urlMiddleware({
        url: (req) => Promise.resolve(SERVER),
      }),
      authMiddleware({
        token: () => localStorage.getItem('userToken')
      }),
      cacheMiddleware({size: 100, ttl: 100000 }),
      //   tokenRefreshPromise: (req) => {
      //     console.log('[client.js] resolve token refresh', req);
      //     return fetch('/jwt/refresh')
      //       .then((res) => res.json())
      //       .then((json) => {
      //         const token = json.token;
      //         store.set('jwt', token);
      //         return token;
      //       })
      //       .catch((err) => console.log('[client.js] ERROR can not refresh token', err));
      //   },
      // })    
      (next) => async (req) => {
        req.fetchOpts.headers['client-name-for-advanced-use-cases'] = 'react web';
        req.fetchOpts.headers['client-version-for-advanced-use-cases']= '1';

        const res = await next(req);
        return res;
      }
    ]);

  const environment = new Environment({
    network: network,
    store: new Store(new RecordSource()),
  });

  export default environment;
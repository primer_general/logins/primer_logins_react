import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import visualizer from 'rollup-plugin-visualizer';
import serve from 'rollup-plugin-serve';

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/bundle.js',
    name: 'bundle',
    format: 'iife',
    sourcemap: 'inline',
    globals: {
      'react': 'React'
    }
  },
  // external: [
  //   'react', 'debug'
  // ],
  plugins: [
    resolve({
      browser: true,
      extensions: [ '.mjs', '.js', '.jsx', '.json' ]
    }),
    babel ({
      // babelrc: false,
      exclude: 'node_modules/**'
    }),
    visualizer(),
    commonjs({
      namedExports: {
        // left-hand side can be an absolute path, a path
        // relative to the current directory, or the name
        // of a module in node_modules
        'react': ['Children', 'Component', 'PropTypes', 'createElement', 'useContext', 'useState'],
        'gud': [ 'default' ],
        'react-is': ['isValidElementType'],
        'relay-runtime': ['Network', 'QueryResponseCache', 'Environment', 'Store', 'RecordSource'],
        'react-relay': ['graphql', 'QueryRenderer']
      }
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    serve('dist')   
  ]
};
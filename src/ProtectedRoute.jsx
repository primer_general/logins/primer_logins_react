// @flow
import React, { useContext } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import type { ContextRouter } from '../flow-typed/npm/react-router-dom_v5.x.x.js'

import { DataContext } from './DataContext';


const ProtectedRoute = (props: Route) => {

    const { component: Component } = props;
    const context = useContext(DataContext);

    return (
        ( context.isAuthenticated() ) ? 
            <Component /> :
            <Redirect to={{
                pathname: '/login',
                state: {
                    from: props.location
                }
            }} />
    )

}

export default ProtectedRoute;
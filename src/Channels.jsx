import React from 'react'
import { graphql, createPaginationContainer } from 'react-relay';
import { uid } from 'react-uid';
import StackGrid, { transitions } from "react-stack-grid";
import sizeMe from 'react-sizeme';
const { scaleDown } = transitions;

import Channel from './Channel'
import { ITEMS_PER_PAGE } from './Constants'

const Channels = (props) => {

    const { 
      size: { 
        width
      } 
    } = props;

    const _loadMore = () => {
        if (!props.relay.hasMore()) {
            console.log(`Nothing more to load`)
            return
        } else if (props.relay.isLoading()) {
            console.log(`Request is already pending`)
            return
        }

        props.relay.loadMore(ITEMS_PER_PAGE)
    }

    return (
        <>
            <StackGrid
                    appear={scaleDown.appear}
                    appeared={scaleDown.appeared}
                    enter={scaleDown.enter}
                    entered={scaleDown.entered}
                    leaved={scaleDown.leaved}
                    columnWidth={width <= 768 ? '100%' : '33.33%'}>    
            {
                props.list.channels.edges.map( ({node}, index) => {
                    return <Channel key={uid(node, index)} item={node} />
                })
            }
            </StackGrid>
            <button onClick={_loadMore}>Load more</button>
        </>
    )
}

export default createPaginationContainer(
sizeMe()(Channels),
{
    list: graphql`
        fragment Channels_list on User
            @argumentDefinitions(
                first: { type: "Int!" }
                after: { type: "String" }
            ) 
            {
                channels(first: $first, after: $after) 
                        @connection(key: "Channels_channels"){
                    totalCount
                    pageInfo {
                        hasNextPage
                        endCursor
                    }
                    edges {
                        cursor
                        node {
                            ...Channel_item
                        }
                    }
                }        
            }
`},
{
    direction: 'forward',
    query: graphql`
        query Channels_Query($first: Int!,
                            $after: String) {
            me {
                ...Channels_list @arguments(first: $first,
                                        after: $after)
            }
        }
    `,
    getConnectionFromProps(props) {
        return props.list && props.list.channels
    },
    getFragmentVariables: (prevVars, totalCount) => {
      return {
        ...prevVars,
        count: totalCount
      }
    },
    getVariables: (props, {count, cursor}, fragmentVariables) => {
      return {
        first: count,
        after: cursor
      }
    }        
})
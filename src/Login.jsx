// @flow
import React, { useContext, useState } from 'react';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GitHubLogin from 'react-github-login';
import * as Sentry from '@sentry/browser';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import type { ContextRouter } from '../flow-typed/npm/rreact-router-dom_v5.x.x.js'

import { DataContext } from './DataContext';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    card: {
        minHeight: 300,
    },
    cardContent: {
        marginTop: '10%'
    },
    title: {
        fontSize: 14,
    },
    gitHubContainer: {
        display: 'flex',
        alignItems: 'center'
    }
}));

const Login = (props: ContextRouter) => {

    const classes = useStyles();

    const [status, setStatus] = useState({});
    const context = useContext(DataContext);
    const { from } = props.location.state || { from : { pathname: '/home'} };

    const responseGoogle = (response) => {
        if( response.error ) {
            console.error(response.error);
        } else {
            context.updateCode(response.code, "google", (error) => {
                if( !error )  {  
                    Sentry.captureMessage('Logged In');
                    props.history.push(from.pathname);
                } else {
                    setStatus(error);  
                }    
            });
        }
    }

    const responseGithub = response => {
        if( response.code ) {
            context.updateCode(response.code, "github", (error) => {
                if( !error )    
                    props.history.push(from.pathname);
            })
        }
    }

    const responseFacebook = (response) => {
        if( response.accessToken ) {
            context.updateToken(response.accessToken);
            props.history.push(from.pathname);
        }
    }     

   return (
       <Container component="main" className={classes.root} maxWidth="xs">
        <Card className={classes.card}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                   Please Login
                </Typography>

                <Grid container direction="row" spacing={3} className={classes.cardContent}>
                        <Grid item xs>
                            <GoogleLogin
                                clientId="1090129621459-7af5mkpi94pcfr0ctb47ssbojphh01bm.apps.googleusercontent.com"
                                responseType='code'
                                accessType='offline'
                                onSuccess={responseGoogle}
                                onFailure={responseGoogle}
                                style={{cursor: 'pointer'}}
                                render={ renderProps => (
                                    <span className='loginButton'>
                                        <FontAwesomeIcon icon={['fab', 'google']} size="2x" color='#F74933' 
                                            className='loginIcon' 
                                            onClick={renderProps.onClick}
                                            disabled={renderProps.disabled}/>
                                    </span>    
                                )}
                                cookiePolicy={'single_host_origin'}>
                            </GoogleLogin>   
                        </Grid>
                        <Grid item xs>
                            <FacebookLogin
                                appId="724046081359118"
                                fields="name,email,picture"
                                callback={responseFacebook} 
                                responseType='code'
                                render={renderProps => (
                                    <span className='loginButton'>
                                        <FontAwesomeIcon icon={['fab', 'facebook']} size="2x" color='#455CA8' 
                                            className='loginIcon'
                                            onClick={renderProps.onClick}
                                            disabled={renderProps.disabled}/>
                                    </span>    
                                )}>
                            </FacebookLogin>    
                        </Grid>
                        <Grid item xs className={classes.gitHubContainer}>
                            <GitHubLogin clientId="b8165760bed2aee3b367"
                                redirectUri=""
                                className='loginButton gitHubButton'
                                onSuccess={responseGithub}
                                onFailure={responseGithub}>
                                    <span className='loginIcon'>
                                        <FontAwesomeIcon icon={['fab', 'github']} size="3x" color='#403A3A' 
                                                        className='loginIcon'/>
                                    </span>
                            </GitHubLogin>    
                        </Grid>
                        
                </Grid>
            </CardContent>
        </Card> 
       </Container>
   )
}

export default Login;
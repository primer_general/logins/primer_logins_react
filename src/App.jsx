// @flow
import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';

import environment from './Environment';

import { DataProvider } from './DataContext';
import ProtectedRoute from './ProtectedRoute';
import Home from './Home';
import Profile from './Profile';
import Login from './Login';
import AuthorizeMutation from './mutations/AuthorizeMutation';

library.add(fab);

const App = () => {

    const user = {
        code: null,
        updateToken: function (token) {
            this.token = token.id_token;
            this.tokenType = token.token_type;
            localStorage.setItem('userToken', this.token);
            localStorage.setItem('tokenType', this.tokenType);
        },
        updateCode: function(code, provider, cb) {
            this.code = code;
            AuthorizeMutation.commit(environment, code, provider, cb);
        },        
        isAuthenticated: function() {
            return this.code != null;
        },
    }

    return(
        <DataProvider value={user}>
            <div>Primer Logins</div>
            <Switch>
                <Route path="/login" component={Login} />
                <ProtectedRoute exact path='/' component={Home} />
                <ProtectedRoute exact path='/home' component={Home} />
                <ProtectedRoute path='/profile' component={Profile} />
            </Switch>
        </DataProvider>
    )
}

export default App;
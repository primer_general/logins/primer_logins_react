// @flow
import React, { useContext } from "react";
import {graphql, QueryRenderer } from 'react-relay';
import type { ProfileQueryResponse } from './__generated__/ProfileQuery.graphql.js'

import { DataContext } from './DataContext';
import environment from './Environment';

const ProfileQuery = graphql`
    query ProfileQuery {
        me {
            name
            email
            pictureUrl
        }
    }
`;

const Profile = () => {

    const context = useContext(DataContext);
    console.log(context);

    return (
        <QueryRenderer
            environment={environment}
            query={ProfileQuery}
            render={ ({error, props}) => {
                if (error) {
                    return <div>{error.message}</div>;
                }
                if (!props) {
                    return (<div>Loading...</div>);
                }

                return (<>
                    <div>{ props.me.name }</div>
                    <img src={props.me.pictureUrl} />
                </>)
            }}            
        />
    )
}

export default Profile;
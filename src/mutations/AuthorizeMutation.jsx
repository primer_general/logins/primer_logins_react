// @flow
import {commitMutation, graphql} from 'react-relay';

import type { Environment } from '../../flow-typed/npm/react-relay_v1.x.x.js';
import type { AuthorizeMutation, 
              AuthorizeMutationVariables 
} from './__generated__/AuthorizeMutation.graphql.js';

type AuthorizeCallback = (error: string | null) => void

const mutation = graphql`
mutation AuthorizeMutation($code: String!, $providerName: String!) {
  authorize(code: $code, providerName: $providerName) {
      name
      email
      token
  }
}  
`;

const commit = (environment: Environment, code: string, provider: string, cb: AuthorizeCallback)=> {

    const variables: AuthorizeMutationVariables = {
        code: code,
        providerName: provider
    };
  
    commitMutation(environment,
        {
            mutation,
            variables,
            updater: (proxyStore) => {
            },
            onError: (error) => {
              console.error(error);
              if( cb ) {
                cb(error.message)
              }
            },
            onCompleted: (response, errors) => {
                if( errors && errors.length > 0 ) {
                  errors.map( error => {
                    console.error(error.message);
                  })
                  if( cb ) cb('multiple errors')
                } else {
                  console.log(response.authorize);
                  localStorage.setItem('userToken', response.authorize.token);
                  localStorage.setItem('tokenType', 'Bearer');
                  if( cb ) cb(null);
                }
              }            
    });
}

export default {commit};

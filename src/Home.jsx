// @flow
import React, { useState } from 'react';
import {graphql, QueryRenderer} from 'react-relay';

import environment from './Environment';
import Channels from './Channels';
import Channel from './Channel';
import { ITEMS_PER_PAGE } from './Constants'

const HomeQuery = graphql`
  query HomeScreen_Query($first: Int!, $after: String) {
    me {
        ...Channels_list @arguments (first: $first,
                                     after: $after)   
    }    
  }
`;

const Home = (props) => {

    return (
       <QueryRenderer
              environment={environment}
              query={HomeQuery}
              variables={{
                  "first": ITEMS_PER_PAGE
              }}
              render={ ({error, props}) => {
                  if (error) {
                      return <div>{error.message}</div>;
                  }
                  if (!props) {
                      return (<div>Loading...</div>);
                  }                  

                  return <Channels list={props.me} />
              }}
        />      
    )
}

export default Home;